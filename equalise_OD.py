metadata = {
    'protocolName': 'Toggle lights',
    'author': 'Phil <phil@singerinstruments.com>',
    'description': '''Protocol to equalise OD600 of a MWP.
    Measurements were generated with a BMG spectrostar nano. Use the layout
    feature in the BMG data suitefeature to label blank wells or remove these
    rows from the CSV file.Copy and paste the CSV file into the python file.''',
    'apiLevel': '2.11'
    }
#set the final OD600 required
target_OD = 0.4
target_volume = 150

OD600_vals_raw = '''
Well,Content,Raw Data (620)
A01,Sample X1,3.5
A02,Sample X2,3.5
A03,Sample X3,3.5
A04,Sample X4,3.5
A05,Sample X5,3.5
A06,Sample X6,3.5
A07,Sample X7,3.5
A08,Sample X8,3.5
A09,Sample X9,3.5
A10,Sample X10,3.5
A11,Sample X11,3.342
A12,Sample X12,3.5
B01,Sample X13,3.118
B02,Sample X14,2.878
B03,Sample X15,3.006
B04,Sample X16,3.093
B05,Sample X17,2.968
B06,Sample X18,3.045
B07,Sample X19,2.643
B08,Sample X20,2.683
B09,Sample X21,2.933
B10,Sample X22,2.811
B11,Sample X23,2.877
B12,Sample X24,2.997
C01,Sample X25,1.875
C02,Sample X26,2.156
C03,Sample X27,2.153
C04,Sample X28,1.928
C05,Sample X29,1.864
C06,Sample X30,2.207
C07,Sample X31,1.831
C08,Sample X32,1.926
C09,Sample X33,2.06
C10,Sample X34,2.054
C11,Sample X35,1.832
C12,Sample X36,1.852
D01,Sample X37,1.171
D02,Sample X38,1.244
D03,Sample X39,1.2
D04,Sample X40,1.35
D05,Sample X41,1.27
D06,Sample X42,1.3
D07,Sample X43,1.296
D08,Sample X44,1.52
D09,Sample X45,1.272
D10,Sample X46,1.519
D11,Sample X47,1.211
D12,Sample X48,1.462
E01,Sample X49,0.642
E02,Sample X50,0.624
E03,Sample X51,0.686
E04,Sample X52,0.632
E05,Sample X53,0.582
E06,Sample X54,0.658
E07,Sample X55,0.695
E08,Sample X56,0.638
E09,Sample X57,0.625
E10,Sample X58,0.566
E11,Sample X59,0.691
E12,Sample X60,0.75
G01,Blank B,0.1
G02,Blank B,0.099
G03,Blank B,0.102
G04,Blank B,0.105
G05,Blank B,0.098
G06,Blank B,0.122
G07,Blank B,0.1
G08,Blank B,0.097
G09,Blank B,0.106
G10,Blank B,0.097
G11,Blank B,0.1
G12,Blank B,0.104
H01,Blank B,0.093
H02,Blank B,0.104
H03,Blank B,0.105
H04,Blank B,0.104
H05,Blank B,0.101
H06,Blank B,0.099
H07,Blank B,0.097
H08,Blank B,0.114
H09,Blank B,0.104
H10,Blank B,0.096
H11,Blank B,0.105
H12,d,0.1
'''

import csv
from opentrons import protocol_api


OD600_vals = OD600_vals_raw.splitlines()[1:] # Discard the blank first line.
csv_reader = csv.DictReader(OD600_vals)

wells = []
contents = []
values = []

for csv_row in csv_reader:
    wells.append(csv_row['Well'])
    contents.append(csv_row['Content'])
    values.append(float(csv_row['Raw Data (620)']))

#strip out 0s after the row letter
for i, well in enumerate(wells):
    if well[1] == '0': wells[i] = well[0]+well[2]

#remove all blanks
not_blank_index = [i for i, x in enumerate(contents) if x != "Blank B"]
contents = [contents[i] for i in not_blank_index]
wells = [wells[i] for i in not_blank_index]
values = [values[i] for i in not_blank_index]
dilution_coefs = [target_OD/value for value in values]
culture_volumes = [round(target_volume*dilution_coef,2) for dilution_coef in dilution_coefs]
for i, culture_volume in enumerate(culture_volumes):
    if culture_volume > target_volume: culture_volume = target_volume
    culture_volumes[i] = culture_volume
pbs_volumes = [round(target_volume - culture_volume,2) for culture_volume in culture_volumes]



def run(protocol: protocol_api.ProtocolContext):

    def drop_all_tips():
        if protocol.loaded_instruments['left'].has_tip:
            protocol.loaded_instruments['left'].drop_tip()
        if protocol.loaded_instruments['right'].has_tip:
            protocol.loaded_instruments['right'].drop_tip()

    # turn on robot rail lights
    protocol.set_rail_lights(on = True)
    #set consumables
    tiprack_20 = protocol.load_labware('opentrons_96_filtertiprack_20ul', 10)
    tiprack_200 = protocol.load_labware('opentrons_96_filtertiprack_200ul', 11)
    initial_culture_plate = protocol.load_labware('thermop106reuse_384_wellplate_145ul', 3)
    final_culture_plate = protocol.load_labware('thermop106reuse_384_wellplate_145ul', 4)
    resevoir = protocol.load_labware('ourgeneric_24_reservoir_8000ul', 6) # need to change
    #set pipettes
    pipette_20 = protocol.load_instrument('p20_single_gen2', mount='left',tip_racks=[tiprack_20])
    pipette_300 = protocol.load_instrument('p300_single_gen2', mount='right',tip_racks=[tiprack_200])

    #dispense PBS
    for i, well in enumerate(wells):
        pbs_vol = pbs_volumes[i]
        if pbs_vol < 30:
            pipette =  pipette_20
        if pbs_vol >= 30:
            pipette =  pipette_300
        if not pipette.has_tip:
            pipette.pick_up_tip() # picks up tip_rack:A1
        pipette.transfer(pbs_vol, resevoir['A1'],
                                           final_culture_plate[well], new_tip = 'never')
    #drop_tip for both pipettes
    drop_all_tips()


    #dispense culture
    for i, well in enumerate(wells):
        culture_vol = culture_volumes[i]
        if culture_vol < 30:
            pipette =  pipette_20
        if culture_vol >= 30:
            pipette =  pipette_300
        #mix**********
        pipette_300.pick_up_tip()
        pipette_300.mix(repetitions=4, location  = initial_culture_plate[well])
        if not pipette.has_tip:
            pipette.pick_up_tip() # picks up tip_rack:A1
        pipette.transfer(pbs_vol, initial_culture_plate[well],
                           final_culture_plate[well].top(), new_tip = 'never',
                           touch_tip = True)
        drop_all_tips()


    protocol.set_rail_lights(on = False)
    protocol.comment('Done! Your culture is equalised.')
    protocol.comment(str(round(sum(pbs_volumes)))+'µL of PBS used')


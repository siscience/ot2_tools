metadata = {
    'protocolName': 'CSV maps 1.1',
    'author': 'Phil <phil@singerinstruments.com>',
    'description': '''Test protocol to use CSV maps to dispense liquids.''',
    'apiLevel': '2.11'
    }


#The first row of CSV file should contain a dictionary of any liquid handling parameters
target_maps = {'h2o': '''{"well_location": "bottom"}
33,33,31.5,31.5,30,30,33,33,31.5,31.5,30,30
28.5,28.5,27,27,25.5,25.5,28.5,28.5,27,27,25.5,25.5
24,24,22.5,22.5,21,21,24,24,22.5,22.5,21,21
19.5,19.5,18,18,16.5,16.5,19.5,19.5,18,18,16.5,16.5
15,15,13.5,13.5,12,12,15,15,13.5,13.5,12,12
10.5,10.5,9,9,7.5,7.5,10.5,10.5,9,9,7.5,7.5
6,6,4.5,4.5,3,3,6,6,4.5,4.5,3,3
1.5,1.5,0,0,81,81,1.5,1.5,0,0,81,81
''',
"media_x10": '''{"well_location": "top", "blow_out": True}
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
15,15,15,15,15,15,15,15,15,15,15,15
''',
"gluc_50":'''{"well_location": "top", "blow_out": True, "touch_tip" : True}
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
54,54,54,54,54,54,54,54,54,54,54,54
''',
"ethanol":'''{"well_location": "top", "blow_out": True}
0,0,1.5,1.5,3,3,0,0,1.5,1.5,3,3
4.5,4.5,6,6,7.5,7.5,4.5,4.5,6,6,7.5,7.5
9,9,10.5,10.5,12,12,9,9,10.5,10.5,12,12
13.5,13.5,15,15,16.5,16.5,13.5,13.5,15,15,16.5,16.5
18,18,19.5,19.5,21,21,18,18,19.5,19.5,21,21
22.5,22.5,24,24,25.5,25.5,22.5,22.5,24,24,25.5,25.5
27,27,28.5,28.5,30,30,27,27,28.5,28.5,30,30
31.5,31.5,33,33,0,0,31.5,31.5,33,33,0,0
''',
"culture_no_uv":'''{'new_tip' : 'never',"well_location": "top", "blow_out": True}
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,48,48,0,0,0,0,0,0
48,48,48,48,0,0,0,0,0,0,0,0
''',
"culture_uv":'''{'new_tip' : 'never',"well_location": "top", "blow_out": True}
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,48,48
0,0,0,0,0,0,48,48,48,48,0,0
'''}


source_maps = {'h2o': '''
3000,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
''',
"media_x10": '''
0,0,2500,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
''',
"gluc_50":'''
0,0,0,6200,0,0
0,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
''',
"ethanol":'''
0,0,0,0,2000,0
0,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
''',
"culture_no_uv":'''{"big_mix" : True}
0,0,0,0,0,3000
0,0,0,0,0,0
0,0,0,0,0,0
0,0,0,0,0,0
''',
"culture_uv":'''{"big_mix" : True}
0,0,0,0,0,0
0,0,0,0,0,3000
0,0,0,0,0,0
0,0,0,0,0,0
'''}

import string
from opentrons import protocol_api
import ast


def csv_map_parser(map_csv):
    plate_params = map_csv.splitlines()[0]#del
    if(len(plate_params)==0): plate_params_dict = {}
    else: plate_params_dict = ast.literal_eval(plate_params)#del
    plate_params = map_csv.splitlines()[0]
    csv_as_list = map_csv.splitlines()[1:] # Discard the blank first line.
    csv_as_list = [sublist.split(",") for sublist in csv_as_list]
    rows = len(csv_as_list)
    row_letters = string.ascii_uppercase[:rows]
    cols = len(csv_as_list[0])
    linear_map = [float(element) for sublist in csv_as_list for element in sublist]
    cardinals = [row+str(col) for row in row_letters for col in range(1,cols+1)]
    #remove all 0
    not_zero_index = [i for i, x in enumerate(linear_map) if x != 0]
    vols_to_dispence = [linear_map[i] for i in not_zero_index]
    coords_to_dispence = [cardinals[i] for i in not_zero_index]
    dict_to_return = {'volumes': vols_to_dispence,
            'cardinals': coords_to_dispence}
    dict_to_return.update(plate_params_dict)
    return(dict_to_return)


def run(protocol: protocol_api.ProtocolContext):

    def drop_all_tips():
        if protocol.loaded_instruments['left'].has_tip:
            protocol.loaded_instruments['left'].drop_tip()
        if protocol.loaded_instruments['right'].has_tip:
            protocol.loaded_instruments['right'].drop_tip()

    def source_target_transfer(source_dict, target_dict, source_plate, target_plate, min_source_well_vol = 0):
        for liquid_map in target_dict:
            source = csv_map_parser(source_dict[liquid_map])
            targets = csv_map_parser(target_dict[liquid_map])

            #defaults for transfer parameters
            transfer_params = {'new_tip' : 'never', #always or never
                               'touch_tip': False,
                               'blow_out' : False,
                               "blowout_location" : 'destination well',
                               "disposal_volume_perc" : 0,
                               "big_mix" : False} #True has problems with always new_tip
            transfer_params.update(targets)
            transfer_params.update(source)

            #mix if needed

            if transfer_params['big_mix']:
                for source_i, source_cardinal in enumerate(source['cardinals']):
                    if not pipette_300.has_tip: pipette_300.pick_up_tip()
                    pipette_300.mix(repetitions=3, location  = source_plate[source_cardinal])
                drop_all_tips()




            for i, well in enumerate(targets['cardinals']):
                target_vol = targets['volumes'][i]
                for source_i, source_volume in enumerate(source['volumes']):
                    if source_volume-target_vol > min_source_well_vol:
                        source_cardinal = source['cardinals'][source_i]
                        break


                if target_vol < 30:
                    pipette =  pipette_20
                    disposal_volume = (transfer_params['disposal_volume_perc']/100)*20
                if target_vol >= 30:
                    pipette =  pipette_300
                    disposal_volume = (transfer_params['disposal_volume_perc']/100)*300
                if not pipette.has_tip and transfer_params['new_tip'] == 'never':
                    pipette.pick_up_tip() # picks up tip_rack:A1

                #change target location
                if 'well_location' in targets:
                    if targets['well_location'] == "top":
                        target_well = target_plate[well].top()
                    if targets['well_location'] == "center":
                        target_well= target_plate[well].center()
                    if targets['well_location'] == "bottom":
                        target_well= target_plate[well].bottom()
                else: target_well = target_plate[well].bottom()

                #change source location
                if 'well_location' in source:
                    if source['well_location'] == "top":
                        source_well = source_plate[source_cardinal].top()
                    if source['well_location'] == "center":
                        source_well = source_plate[source_cardinal].center()
                    if source['well_location'] == "bottom":
                        source_well = source_plate[source_cardinal].bottom()
                else: source_well = source_plate[source_cardinal].bottom()


                pipette.transfer(target_vol, source_well,
                                     target_well,
                                     new_tip = transfer_params['new_tip'],
                                     touch_tip = transfer_params['touch_tip'],
                                     blow_out = transfer_params['blow_out'],
                                     blowout_location = transfer_params['blowout_location'],
                                     disposal_volume = disposal_volume)
                #substract volume from source
                source['volumes'][source_i] = source['volumes'][source_i]- target_vol
                protocol.comment(liquid_map+ ': '+
                                 str((source['volumes']))+
                                 ' ul left in source')
                #drop_tip for both pipettes
                if transfer_params['new_tip'] == 'always' or i == len(targets['cardinals'])-1:
                    drop_all_tips()

    # turn on robot rail lights
    protocol.set_rail_lights(on = True)
    #set consumables
    tiprack_20 = protocol.load_labware('opentrons_96_tiprack_20ul', 9)
    tiprack_200 = protocol.load_labware('fisherbrandtipot2_96_tiprack_200ul', 10)
    tiprack_200_2 = protocol.load_labware('fisherbrandtipot2_96_tiprack_200ul', 11)
    target96 = protocol.load_labware('kartell_96_wellplate_200ul', 3)
    source24 = protocol.load_labware('ourgeneric_24_reservoir_8000ul', 6) # need to change
    #set pipettes
    pipette_20 = protocol.load_instrument('p20_single_gen2', mount='left',tip_racks=[tiprack_20])
    pipette_300 = protocol.load_instrument('p300_single_gen2', mount='right',tip_racks=[tiprack_200, tiprack_200_2])

    source_target_transfer(source_maps, target_maps, source24,
                           target96, min_source_well_vol =100)

    #drop_tip for both pipettes
    drop_all_tips()


    protocol.set_rail_lights(on = False)

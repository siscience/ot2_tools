# README #

The repo is intended to host all python related tools and scripts developed
for Opentons2 (OT2). Currently, all tools were written in the 2nd version of
the python API.

### How do I get set up? ###

* Clone this repo in SourceTree
* Edit and create new OT2 protocol .py files in the directory
* Commit and push changes through SourceTree
* Some protocols require custom labware. Download Google Drive. In the OT2 app,
locate '... more' > 'CUSTOM LABWARE' > 'Change source' and point to 
Internal Reports\Opentrons 2\Consumables folder in Google Drive. The custom 
labware will now be available to the API
* Import and run protocol into the OT2 app


### Tips and tricks ###

* The jupyter notebook hosted on the OT2 works but becuase it bypasses the
calibration steps it is not reccomended. Instead, import .py files as protocols.
This is subject to change
* Under construction



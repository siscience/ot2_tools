from opentrons import protocol_api


metadata = {
    'protocolName': 'Toggle lights',
    'author': 'Phil <phil@singerinstruments.com>',
    'apiLevel': '2.11'
    }

def run(protocol: protocol_api.ProtocolContext):
    # turn on robot rail lights
    protocol.set_rail_lights(on = not protocol.rail_lights_on)
    tiprack = protocol.load_labware('opentrons_96_tiprack_20ul', 2)
    plate = protocol.load_labware('corning_96_wellplate_360ul_flat', 3)
    pipette = protocol.load_instrument('p20_single_gen2', mount='left',tip_racks=[tiprack])
    pipette.pick_up_tip() # picks up tip_rack:A1
    pipette.return_tip()
    pipette.pick_up_tip() # picks up tip_rack:B1
    pipette.return_tip()
    protocol.set_rail_lights(on = False)